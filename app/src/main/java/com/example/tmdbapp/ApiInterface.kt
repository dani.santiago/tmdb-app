package com.example.tmdbapp

import com.example.tmdbapp.model.Cast
import com.example.tmdbapp.model.Data
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

const val apiKey: String = "api_key=7ddf7a9a9418aededef2a54f76ecf89b"

interface ApiInterface {

    @GET("discover/movie?$apiKey")
    suspend fun getData(@Query("page") page: Int): Response<Data>

    @GET("movie/{id}/credits?$apiKey")
    suspend fun getCast(@Path("id") movieId: Int): Response<Cast>

    companion object {
        val BASE_URL = "https://api.themoviedb.org/3/"

        fun create(): ApiInterface {
            val client = OkHttpClient.Builder().build()
            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()

            return retrofit.create(ApiInterface::class.java)
        }
    }
}