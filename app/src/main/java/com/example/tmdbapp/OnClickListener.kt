package com.example.tmdbapp

import com.example.tmdbapp.model.Movie

interface OnClickListener {
    fun onClick(movie: Movie)
}