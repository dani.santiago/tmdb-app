package com.example.tmdbapp.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.tmdbapp.R
import com.example.tmdbapp.databinding.ActorItemBinding
import com.example.tmdbapp.model.Actor

class ActorsAdapter (private val actors: List<Actor>): RecyclerView.Adapter<ActorsAdapter.ViewHolder>() {

    private lateinit var context: Context

    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        val binding = ActorItemBinding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.actor_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val actor = actors[position]
        with(holder){
            binding.tvName.text = actor.name
            Glide.with(context)
                .load("https://image.tmdb.org/t/p/w500/${actor.profile_path}")
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .circleCrop()
                .into(binding.image)
        }
    }

    override fun getItemCount(): Int {
        return actors.size
    }
}