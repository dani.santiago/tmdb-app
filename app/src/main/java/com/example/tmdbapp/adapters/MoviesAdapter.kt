package com.example.tmdbapp.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.tmdbapp.OnClickListener
import com.example.tmdbapp.R
import com.example.tmdbapp.databinding.MovieItemBinding
import com.example.tmdbapp.model.Movie

class MoviesAdapter (private val movies: List<Movie>, private val listener: OnClickListener): RecyclerView.Adapter<MoviesAdapter.ViewHolder>() {

    private lateinit var context: Context

    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        val binding = MovieItemBinding.bind(view)
        fun setListener(movie: Movie){
            binding.root.setOnClickListener {
                listener.onClick(movie)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.movie_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val movie = movies[position]
        with(holder){
            setListener(movie)
            binding.tvTitle.text =movie.title
            binding.cbFavorite.isChecked = false
            Glide.with(context)
                .load("https://image.tmdb.org/t/p/w500/${movie.backdropPath}")
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .circleCrop()
                .into(binding.image)
        }
    }

    override fun getItemCount(): Int {
        return movies.size
    }

}