package com.example.tmdbapp

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.tmdbapp.model.Cast
import com.example.tmdbapp.model.Data
import com.example.tmdbapp.model.Movie
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MoviesViewModel: ViewModel() {
    val repository = Repository()
    var data = MutableLiveData<Data>()
    var actualMovie = MutableLiveData<Movie>()
    var cast = MutableLiveData<Cast>()
    var currentPage: Int = 1

    init {
        fetchData("")
    }

    fun fetchData(page: String){
        viewModelScope.launch {
            if(page == "next"){
                currentPage++
            }
            else if(page == "previous"){
                currentPage--
            }
            val response = withContext(Dispatchers.IO) { repository.fetchData(currentPage) }
            if(response.isSuccessful){
                data.postValue(response.body())
            }
            else{
                Log.e("Error :", response.message())
            }
        }
    }

    fun setMovie(movie: Movie){
        actualMovie.value = movie
        viewModelScope.launch {
            val response = withContext(Dispatchers.IO) { repository.fetchCast(movie.id) }
            if(response.isSuccessful){
                cast.postValue(response.body())
            }
            else{
                Log.e("Error :", response.message())
            }
        }
    }
}