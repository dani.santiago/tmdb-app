package com.example.tmdbapp

class Repository {

    private val apiInterface = ApiInterface.create()

    suspend fun fetchData(page: Int) = apiInterface.getData(page)

    suspend fun fetchCast(idMovie: Int) = apiInterface.getCast(idMovie)

}