package com.example.tmdbapp.model

data class Cast(
    val cast: List<Actor>,
    val id: Int
)