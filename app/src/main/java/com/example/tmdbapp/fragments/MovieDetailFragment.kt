package com.example.tmdbapp.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.tmdbapp.MoviesViewModel
import com.example.tmdbapp.adapters.ActorsAdapter
import com.example.tmdbapp.adapters.MoviesAdapter
import com.example.tmdbapp.databinding.FragmentMovieDetailBinding

class MovieDetailFragment : Fragment() {

    lateinit var binding: FragmentMovieDetailBinding
    private lateinit var actorsAdapter: ActorsAdapter
    private val viewModel: MoviesViewModel by activityViewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentMovieDetailBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.actualMovie.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            binding.tvTitle.text = it.title
            binding.tvRuntime.text = "Runtime:"
            binding.tvReleaseDate.text = "Release date: ${it.releaseDate}"
            binding.tvPopularity.text = "Popularity: ${it.popularity}"
            binding.tvOverview.text = it.overview
            Glide.with(context!!)
                .load("https://image.tmdb.org/t/p/w500/${it.backdropPath}")
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .into(binding.image)
        })
        viewModel.cast.observe(viewLifecycleOwner, Observer {
            actorsAdapter = ActorsAdapter(it.cast)
            binding.recyclerView.apply {
                setHasFixedSize(true) //Optimitza el rendiment de l’app
                layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                adapter = actorsAdapter
            }
        })
    }

}