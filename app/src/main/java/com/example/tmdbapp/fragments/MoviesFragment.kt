package com.example.tmdbapp.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.tmdbapp.MoviesViewModel
import com.example.tmdbapp.OnClickListener
import com.example.tmdbapp.R
import com.example.tmdbapp.adapters.MoviesAdapter
import com.example.tmdbapp.databinding.FragmentMoviesBinding
import com.example.tmdbapp.model.Data
import com.example.tmdbapp.model.Movie

class MoviesFragment : Fragment(), OnClickListener {

    lateinit var binding: FragmentMoviesBinding
    private lateinit var moviesAdapter: MoviesAdapter
    private val viewModel: MoviesViewModel by activityViewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentMoviesBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.data.value?.let {
            setUpRecyclerView(it)
        }
        viewModel.data.observe(viewLifecycleOwner, Observer {
            if(viewModel.data.value == null){
                Toast.makeText(context, "ERROR", Toast.LENGTH_SHORT).show()
            }
            else {
                println("ESTIC A LA PÀGINA ${viewModel.currentPage}")
                setUpRecyclerView(it)
            }
        })
        binding.recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if(!binding.recyclerView.canScrollVertically(1)){
                    //viewModel.fetchData("next")
                }
            }
        })
        binding.swipeRefresh.setOnRefreshListener {
            if(viewModel.currentPage>1){
                viewModel.fetchData("previous")
            }
            binding.swipeRefresh.isRefreshing = false
        }
    }

    fun setUpRecyclerView(myData: Data){
        moviesAdapter = MoviesAdapter(myData.results, this)
        binding.recyclerView.apply {
            setHasFixedSize(true) //Optimitza el rendiment de l’app
            layoutManager = LinearLayoutManager(context)
            adapter = moviesAdapter
        }
    }

    override fun onClick(movie: Movie) {
        viewModel.setMovie(movie)
        findNavController().navigate(R.id.action_moviesFragment_to_movieDetailFragment)
    }
}